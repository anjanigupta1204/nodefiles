/**
 * http://usejsdoc.org/
 */
"use strict";
var express = require('express');
var async = require("async");
var oauthSignature = require('oauth-signature');
var router = express.Router();
var path = require('path');
var successCode = 'success';
var errorCode = 'error';
var failureCode = 'failure';
var http = require('http');
var request = require('request');
var fs = require('fs');
var multer = require('multer');
var querystring = require('querystring');
var twitterAPI = require('node-twitter-api');


//local server
var _host = '192.168.1.89';
var _port = '8080';

//live server
//var _host = '14.141.23.102';
//var _port = '8080';

function modifyString(inputString, replacingChar, replaceWith) {
	var str = inputString.split(replacingChar).join(replaceWith);
	return str;
}

function getRequestHeader(req, content = true) {
	var _headers;
	if (req.headers['x-auth-header'] && req.headers['x-auth-header'].length) {
		if (content) {
			_headers = {
				'Content-Type': 'application/json',
				'X-AUTH-HEADER': req.headers['x-auth-header'],
			}
		} else {
			_headers = {
				'X-AUTH-HEADER': req.headers['x-auth-header'],
			}
		}
	} else {
		_headers = {
			'Content-Type': 'application/json',
		}
	}
	console.log('header:: ', _headers)
	return _headers;
}

function mediaRquesthandle(methodType, req, res) {
	var action = modifyString(req.body.action, '_', '/');
	var data = req.body.rawBody;
	var dataKey = req.body.rawBodyKey;
	var fileName = req.body.file;
	var fileKey = req.body.fileKey;
	var opts = {
		url: 'http://' + _host + ':' + _port + '/' + action,
		method: methodType,
		headers: getRequestHeader(req, false),
		json: true,
		formData: getFormData(dataKey, fileKey, data, fileName)

	}
	console.log('opts send: ', opts);
	var str = '';

	request(opts, function (err, resp, body) {
		if (!err) {
			if (fileName != null && typeof fileName != 'undefined') {
				fs.unlinkSync(path.join(__dirname, '../uploads', fileName));
			}

			resp.setEncoding('utf8');
			resp.on('data', function (chunk) {
				str += chunk;
			});
			res.send(body);

		}
	});
}

function getFormData(dataKey, fileKey, data, file) {

	switch (dataKey) {
		case 'user':
			var fileSet = null;
			var formData;
			if (file != null && file != undefined) {
				formData = {
					logo: fs.createReadStream(__dirname + '/../uploads/' + file),
					//logo: file,
					user: JSON.stringify(data)
				};
			} else {
				formData = {
					user: JSON.stringify(data)
				};
			}
			break;

		case 'media':
			var fileSet = null;
			var formData;
			if (file != null && file != undefined) {
				formData = {
					files: fs.createReadStream(__dirname + '/../uploads/' + file),
					//logo: file,
					media: JSON.stringify(data)
				};
			} else {
				formData = {
					media: JSON.stringify(data)
				};
			}
			break;

		case 'client':
			delete data.startDateString;
			delete data.endDateString;
			var formData;
			if (file != null && file != undefined) {
				formData = {
					logo: fs.createReadStream(__dirname + '/../uploads/' + file),
					client: JSON.stringify(data)
				};
			} else {
				formData = {
					client: JSON.stringify(data)
				};
			}
			break;

		case 'brand':
			var formData;
			if (file != null && file != undefined) {
				formData = {
					logo: fs.createReadStream(__dirname + '/../uploads/' + file),
					brand: JSON.stringify(data)
				};
			} else {
				formData = {
					brand: JSON.stringify(data)
				};
			}
			break;

		default:
			break;
	}
	console.log('~~~~~~~~~~~~', formData, '##########################');
	return formData;

}


var storage = multer.diskStorage({ //multers disk storage settings
	destination: function (req, file, cb) {
		cb(null, './uploads/');
	},

	filename: function (req, file, cb) {
		var datetimestamp = Date.now();
		//console.log('filename: ', file);
		cb(null, 'socioseer-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
		//cb(null, file.originalname);
	}
});

var upload = multer({ //multer settings
	storage: storage
}).single('file');


router.route('')
	.post(function (req, res) {
		var dataArray = JSON.stringify(req.body.myparam);
		console.log('____POST____', dataArray, '____POSTED____');
		var action = modifyString(req.body.action, '_', '/');
		var options = {
			host: _host,
			port: _port,
			path: '/' + action,
			method: 'POST',
			headers: getRequestHeader(req)
		};
		var str = '';
		var httpreq = http.request(options, function (response) {
			response.setEncoding('utf8');
			response.on('data', function (chunk) {
				//res.send(JSON.stringify(chunk));
				str += chunk;
			});
			response.on('end', function () {
				res.send(str);
				//res.send(JSON.stringify(data));
			})
		});

		httpreq.on('error', (e) => {
			console.log(`problem with request: ${e.message}`);
		});
		httpreq.write(dataArray);
		httpreq.end();

	})


router.route('/image').post(function (req, res) {
	console.log('media request :: ', req.body);
	upload(req, res, function (err) {
		if (err) {
			try{
			res.json({
				error_code: 1,
				err_desc: err
			});

			} catch(err){
			res.json({
				error_code: 1,
				err_desc: err
			});

			}
			return;
		}

		res.json({
			status: 200,
			success_message: 'uploaded successfully',
			error_code: 0,
			upload_file_name: req.file.filename
		});
	});
})
router.route('/media')
	.post(function (req, res) {
		console.log('post media request')
		mediaRquesthandle('POST', req, res);
	})


var twitter = new twitterAPI({
	consumerKey: 'MZ2qI9zl9tgyY5G0LDP3oBOih',
	consumerSecret: 'GVR1tIBPTQk8yhWmihE8vVQJJuFm6yG8N9cV8F1XUJVFtQO3H6',
	callback: 'http://192.168.1.175:9000/#!/editClient'
});

router.route('/twitter').post(function (req, res) {

	twitter.getRequestToken(function (error, requestToken, requestTokenSecret, results) {
		if (error) {
			console.log("Error getting OAuth request token : " + error);
		} else {
			console.log('requestToken=>', requestToken, 'requestTokenSecret=>', requestTokenSecret, 'results=>', results);
			/*store token and tokenSecret somewhere, you'll need them later; redirect user */
			var options = { force_login: true };
			var xxxxx = twitter.getAuthUrl(requestToken, options);
			console.log('xx', xxxxx);
			var _response = {
				status: 200,
				data: {
					url: xxxxx,
					requestToken: requestToken,
					requestTokenSecret: requestTokenSecret,
				}
			};
			res.send(_response);
		}
	});

});
router.route('/twitter/access').post(function (req, res) {
	var reqData = req.body.data;
	try {
		twitter.getAccessToken(reqData.newData.oauth_token, reqData.prevData.requestTokenSecret, reqData.newData.oauth_verifier, function (error, accessToken, accessTokenSecret, results) {
			if (error) {
				console.log(error);
			} else {
				console.log('SS', accessToken, accessTokenSecret, results);

				var _prm = { screen_name: results.screen_name };

				twitter.users('show', _prm, accessToken, accessTokenSecret, function (error, _data, response) {
					if (error) {
						console.log(error);
					} else {
						var _response = {
							status: 200,
							data: {
								accessToken: accessToken,
								accessTokenSecret: accessTokenSecret,
								results: results,
								image: _data.profile_image_url,
								fullName: _data.name
							}
						};

						res.send(_response);
					}
				});

			}
		});
	} catch (e) {
		console.log('error::', e);
	}


});

router.route('/twitter/trends').get(function (req, res) {
	var reqData = req.query;
	try {
		var _prm = { id: reqData.woeid };
		twitter.trends('place', _prm, '855706086405439488-NXfPn0i6SMFmQFwpAIEVsWBLyQ7lbFK', 'ZRKd9T7wyKqkkbAkV2D9c2J9YKbvA3SPbvaawAix57mYs', function (error, _data, response) {
			if (error) {
				console.log(error);
			} else {
				var _response = {
					status: 200,
					data: _data[0],
				}
				console.log(_data[0]);
				res.send(_response);
			}
		});
	} catch (e) {
		console.log('error::', e);
	}
})
router.route('/twitter/trendingPost').get(function (req, res) {
	var reqData = req.query;
	try {
		twitter.search(reqData, '855706086405439488-NXfPn0i6SMFmQFwpAIEVsWBLyQ7lbFK', 'ZRKd9T7wyKqkkbAkV2D9c2J9YKbvA3SPbvaawAix57mYs', function (error, _data, response) {
			if (error) {
				console.log(error);
			} else {

				var _response = {
					status: 200,
					data: _data
				}
				console.log(_data);
				res.send(_response);
			}
		});
	} catch (e) {
		console.log('error::', e);
	}
})

router.route('/delete')
	.post(function (req, res) {
		var data = req.body.rawBody;
		var dataKey = req.body.rawBodyKey;
		var action = modifyString(req.body.action, '_', '/');
		var opts = {
			url: 'http://' + _host + ':' + _port + '/' + action,
			method: 'DELETE',
			headers: getRequestHeader(req),
		}
		var str = '';

		request(opts, function (err, resp, body) {
			if (!err) {
				resp.setEncoding('utf8');
				resp.on('data', function (chunk) {
					str += chunk;
				});
				res.send(body);

			}
		});

	})

router.route('/put')
	.post(function (req, res) {
		var data = req.body.rawBody;
		var action = modifyString(req.body.action, '_', '/');
		console.log('1 raw ', req.body)
		//console.log('2 raw ', data.roles)
		var opts = {
			url: 'http://' + _host + ':' + _port + '/' + action,
			method: 'PUT',
			headers: getRequestHeader(req, false),
			json: true,
			body: data
		}
		var str = '';

		request(opts, function (err, resp, body) {
			console.log('bbbb', err, body);
			if (!err) {
				resp.setEncoding('utf8');
				resp.on('data', function (chunk) {
					str += chunk;
				});
				res.send(body);
			}
		});

	})

router.route('/media/put')
	.post(function (req, res) {
		mediaRquesthandle('PUT', req, res);
	})

router.route('')
	.get(function (req, res) {
		try {

			var dataArray = JSON.stringify(req.query);
			var dataArrayObj = req.query;

			var action = modifyString(req.query.action, '_', '/');
			console.log('###urlToHit##', action);
			var queryParam = "";
			var i = 0;
			for (var key in dataArrayObj) {
				var q = "";
				i++;
				if (dataArrayObj.hasOwnProperty(key)) {
					if (Object.keys(dataArrayObj).length == i) {
						q = key + "=" + dataArrayObj[key] + "";
						queryParam += q;
					} else {
						if (key != 'action') {
							q = key + "=" + dataArrayObj[key] + "&";
							queryParam += q;
						}

					}
				}

			}

			var urlToHit = action;
			if (queryParam.length) {
				urlToHit += '?' + encodeURI(queryParam);
			}
			console.log('###urlToHit##', urlToHit);
			var options = {
				host: _host,
				port: _port,
				path: '/' + urlToHit,
				method: 'GET',
				headers: getRequestHeader(req)
			};
			var str = '';
			var httpreq = http.request(options, function (response) {
				response.setEncoding('utf8');
				response.on('data', function (chunk) {
					str += chunk;
				});
				response.on('end', function () {
					res.send(str);
				})
			});
			httpreq.on('error', (e) => {
				console.log(`problem with request: ${e.message}`);
			});

			httpreq.write(dataArray);
			httpreq.end();
		} catch (e) {
			console.log('error::', e);
		}
	})


router.route('/uploads')
	.get(function (req, res) {
		console.log('filename ', req.query.file)
		res.sendFile(path.join(__dirname, '../uploads', req.query.file));
	})


module.exports = router; 